# -*- coding: utf-8 -*-
#fmt: off

import datetime
from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(globals())

extensions += ['lino.sphinxcontrib.logo']

project = 'Lino Voga'
copyright = '2012-{} Rumma & Ko Ltd'.format(datetime.date.today().year)

# intersphinx_mapping['book'] = ('https://www.lino-framework.org', None)
# intersphinx_mapping['ug'] = ('https://using.lino-framework.org', None)
