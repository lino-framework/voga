"""
This package contains apps which are specific to :ref:`voga`.

.. autosummary::
   :toctree:

    cal
    contacts
    courses
    rooms
    roger
    voga

"""
