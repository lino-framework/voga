# Copyright 2013-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import _

# Some translatable strings used in the book

_("Independant")
_("Participation fees")
_("Trips")
_("Journeys")
_("Room renting")
_("Seminars")
_("Excursions")
_("Hikes")
_("Mirrored room")
_("Computer room")
_("Conference room")
_("Outside")
_("Voluntary (flat)")
_("LEA")
