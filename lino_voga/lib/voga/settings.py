# -*- coding: UTF-8 -*-
# Copyright 2012-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.projects.std.settings import *

from lino.api import _

from lino_voga import __version__, intersphinx_urls


class Site(Site):
    verbose_name = "Lino Voga"
    version = __version__
    url = intersphinx_urls['docs']

    migration_class = 'lino_voga.lib.voga.migrate.Migrator'

    # userdocs_prefix = 'voga.'

    user_types_module = 'lino_voga.lib.voga.user_types'
    workflows_module = 'lino_voga.lib.voga.workflows'
    custom_layouts_module = 'lino_voga.lib.voga.layouts'

    demo_fixtures = """std minimal_ledger
    demo demo_bookings payments demo2 checkdata""".split()

    languages = 'en de et'

    show_internal_field_names = True

    # default_build_method = "wkhtmltopdf"
    default_build_method = "appypdf"
    auto_configure_logger_names = 'lino lino_xl lino_voga'

    def get_installed_plugins(self):
        yield super().get_installed_plugins()
        # yield 'lino.modlib.gfks'
        # yield 'lino.modlib.system'
        yield 'lino.modlib.help'
        yield 'lino.modlib.users'
        # yield 'lino_xl.lib.excerpts'
        yield 'lino_xl.lib.countries'
        yield 'lino_voga.lib.contacts'
        yield 'lino_xl.lib.phones'
        yield 'lino_xl.lib.lists'
        yield 'lino_xl.lib.beid'

        yield 'lino.modlib.checkdata'

        yield 'lino_voga.lib.cal'
        yield 'lino_voga.lib.courses'
        yield 'lino_voga.lib.products'
        yield 'lino_voga.lib.rooms'
        yield 'lino_voga.lib.trading'
        yield 'lino_xl.lib.storage'
        yield 'lino_xl.lib.invoicing'

        # yield 'lino.modlib.blacklist'

        # yield 'lino_xl.lib.products'
        # yield 'lino_xl.lib.accounting'
        # yield 'lino_xl.lib.vat'
        # ~ yield 'lino_xl.lib.trading'
        # yield 'lino_cosi.lib.auto.trading'
        yield 'lino_xl.lib.finan'
        yield 'lino_xl.lib.sepa'
        # yield 'lino_xl.lib.bevats'

        # yield 'lino_voga.lib.courses'

        # ~ yield 'lino_xl.lib.households'
        yield 'lino_xl.lib.notes'
        yield 'lino.modlib.uploads'
        # ~ yield 'lino_xl.lib.cal'

        yield 'lino_xl.lib.outbox'
        # ~ yield 'lino_xl.lib.pages'
        # ~ yield 'lino_xl.lib.courses'
        yield 'lino_voga.lib.voga'

        yield 'lino.modlib.export_excel'
        yield 'lino_xl.lib.calview'
        # yield 'lino_xl.lib.extensible'
        yield 'lino.modlib.wkhtmltopdf'  # obsolete
        yield 'lino.modlib.weasyprint'
        yield 'lino_xl.lib.appypod'
        yield 'lino.modlib.changes'
        yield 'lino.modlib.publisher'

    def setup_actions(self):
        super().setup_actions()

        from lino.modlib.changes.utils import watch_changes as wc

        wc(self.models.contacts.Partner)
        wc(self.models.contacts.Person, master_key='partner_ptr')
        wc(self.models.contacts.Company, master_key='partner_ptr')
        wc(self.models.courses.Pupil, master_key='partner_ptr')

    def unused_get_dashboard_items(self, user):
        """Defines the story to be displayed on the admin main page.

        """
        yield self.models.courses.MyCoursesGiven
        yield self.models.courses.StatusReport

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        if True:  # self.is_installed('extensible'):
            # e.g. in lydia6 there is no extensible
            yield ('extensible', 'calendar_start_hour', 9)
            yield ('extensible', 'calendar_end_hour', 21)
        # yield ('vat', 'default_vat_class', 'exempt')
        yield ('products', 'menu_group', 'trading')
        yield ('periods', 'start_year', 2015)
        yield ('invoicing', 'order_model', 'courses.Course')
        # yield ('invoicing', 'three_demo_areas', False)
        yield ('trading', 'print_items_table',
               'trading.ItemsByInvoicePrintNoQtyColumn')

    # def setup_plugins(self):
    #     """
    #     Change the default value of certain plugin settings.
    #
    #     """
    #     if self.is_installed('extensible'):
    #         self.plugins.extensible.configure(calendar_start_hour=9)
    #         self.plugins.extensible.configure(calendar_end_hour=21)
    #     self.plugins.vat.configure(default_vat_class='exempt')
    #     self.plugins.periods.configure(start_year=2015)
    #     self.plugins.products.configure(menu_group="trading")
    #     super(Site, self).setup_plugins()

    def setup_quicklinks(self, user, tb):
        super().setup_quicklinks(user, tb)
        tb.add_action(self.models.courses.Pupils)
        tb.add_action(self.models.courses.Pupils.insert_action,
                      label=_("New {}").format(
                          self.models.courses.Pupil._meta.verbose_name))
