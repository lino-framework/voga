��    A      $  Y   ,      �  
   �     �     �     �     �     �     �     �     �               "     2  	   L  
   V     a     {  
   �     �     �     �     �     �     �     �  
   �     �     �     �            	        "      )  !   J     l     t     �     �  
   �     �     �     �     �     �     �     �     �  
             &     3     C     K     T     ]     i     y     ~     �     �     �     �     �  �  �     s
  
   �
     �
     �
     �
     �
  "   �
     �
     �
     �
                    :     H      X     y  	   �     �     �     �  	   �     �  
   �     �  
   �     �     �  
             !     %     .  $   7  %   \  
   �     �     �     �     �     �  
   �  
   �     �     �  
             !  	   5     ?  	   Q     [     j  	   r     |     �     �     �     �     �     �     �      �             @       7   "   8       5   	       :   %   !       3   +                      =   $          -              
   '   /             <   0           >          6   4   ?   1      .               )             ;   #   2   9          (             *                  ,   A          &                                           Activities Activity Activity type Activity types Amount Certificate Check membership payments Computer room Conference room Confirmation Courses Date of payment Default participation fee Enrolment Enrolments Enrolments using this fee Events Excursions Fee categories Fee category GSM: {0} General Helper Hikes Independant Instructor Instructor type Instructor types Instructors Journeys LEA Legacy ID Member Member until {0} (expected {1}). Member until {0}, but no payment. Members Membership fees Mirrored room New {} Non-member Outside Overview Participant Participant type Participant types Participants Payment info Payment terms Phone: {0} Presence sheet Room renting Scheduled dates Section Sections Seminars Suggestions Summer holidays Time Trips Voluntary (flat) Your enrolments: Your start date [{number}] Renewal {title} {enrolment} to {course} Project-Id-Version: lino-faggio 0.0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2021-07-24 12:49+0300
Last-Translator: Luc Saffre <luc.saffre@gmail.com>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
X-Generator: Poedit 2.0.6
 Aktivitäten Aktivität Aktivitätsart Aktivitätsarten Betrag Zeugnis Mitgliedsbeitragszahlungen prüfen Computersaal Konferenzsaal Bestätigung Kurse Datum der Zahlung Standard-Teilnehmahmegebühr Einschreibung Einschreibungen Einschreibungen mit diesem Tarif Veranstaltungen Ausflüge Tarifkategorien Tarifkategorie GSM: {0} Allgemein Helfer Ausfahrten Selbstständig Kursleiter Kursleiterart Kursleiterarten Kursleiter Reisen LBA Alte Nr. Mitglied Mitglied bis {0} (müsste sein {1}). Mitglied bis {0}, aber keine Zahlung. Mitglieder Mitgliedsbeiträge Spiegelsaal {} erstellen Nicht-Mitglied Draußen Übersicht Teilnehmer Teilnehmerart Teilnehmerarten Teilnehmer Info Buchhaltung Zahlungsbedingungen Tel.: {0} Anwesenheitsblatt Raummiete Geplante Daten Sektion Sektionen Seminare Vorschläge Sommerferien Zeit Reise Ehrenamtlich (pauschal) Ihre Einschreibungen Ihr Beginndatum [{number}] Verlängerung {title} {enrolment} zu {course} 